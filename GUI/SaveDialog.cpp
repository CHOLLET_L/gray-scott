#include <filesystem>
#include <utility>
#include <iostream>

#include "SaveDialog.h"

SaveDialog::SaveDialog(BaseObjectType* cobject, Glib::RefPtr<Gtk::Builder> builder, Gtk::Window* parent)
    : Gtk::FileChooserDialog(cobject), refBuilder(std::move(builder)) {

    set_transient_for(*parent);

    Ftr_all = Glib::RefPtr<Gtk::FileFilter>::cast_dynamic(refBuilder->get_object("Ftr_all"));

    Ftr_Img_png = Glib::RefPtr<Gtk::FileFilter>::cast_dynamic(refBuilder->get_object("Ftr_png"));
    Ftr_Img_jpg = Glib::RefPtr<Gtk::FileFilter>::cast_dynamic(refBuilder->get_object("Ftr_jpg"));
    Ftr_Img_ppm = Glib::RefPtr<Gtk::FileFilter>::cast_dynamic(refBuilder->get_object("Ftr_ppm"));

    Ftr_all->set_name("Tout type de fichier");

    Ftr_Img_png->set_name("Fichier PNG (.png)");
    Ftr_Img_jpg->set_name("Fichier JPEG (.jpg)");
    Ftr_Img_ppm->set_name("Fichier PPM (.ppm)");

    add_filter(Ftr_all);

    add_filter(Ftr_Img_png);
    add_filter(Ftr_Img_jpg);
    add_filter(Ftr_Img_ppm);


    add_button("Annuler", Gtk::RESPONSE_CANCEL);
    add_button("Sauvegarder", Gtk::RESPONSE_OK);

    set_filter(Ftr_all);
}

std::string SaveDialog::get_filename() {
    std::string res = Gtk::FileChooserDialog::get_filename();
    std::string ext;

    if (!std::filesystem::path(res).has_extension()){
        auto filter = get_filter();

        if (filter == Ftr_all)
            filter = Ftr_Img_png;

        // TODO
        // Crappy extraction of extension
        std::string tmp = filter->get_name();
        ext = "." + tmp.substr(tmp.size() - 4, 3);
    }

    return res + ext;
}