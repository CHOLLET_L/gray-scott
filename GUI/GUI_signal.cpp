#include <filesystem>

#include "GUI.h"

void GUI::set_panel_visible(bool visible) {
//    Rev_Panel->set_reveal_child(visible);
// TODO : Find a bug workaround
    Rev_Panel->set_visible(visible);
}

void GUI::bt_Overlay_clicked() {
    set_panel_visible(true);
    buttonOverlay->set_visible(false);
    buttonOverlay->set_reveal_child(false);
}

void GUI::bt_Panel_clicked() {
    set_panel_visible(false);
    buttonOverlay->set_visible(true);
    buttonOverlay->set_reveal_child(true);
}

void GUI::event_product(coord c) {
    solver->add_product(
            std::make_pair(c.first / Gl_con.getGlArea()->get_width(),
                        c.second / Gl_con.getGlArea()->get_height()));

    if(m_SolverWorker.has_stopped()){
        // update only if animation is not running
        // to avoid unnecessary thread wait
        update_widgets();
    }
}

void GUI::cob_preset_modified() {
    adjust_preset_param();
}

void GUI::update_cob_preset() {
    if (active_adj_event)
        if(CoB_preset->get_active_row_number() != 0)
            CoB_preset->set_active(0);
}

void GUI::adjust_preset_param() {
    active_adj_event = false;
    switch(CoB_preset->get_active_row_number()){
        default:
        case 0:
            break;
        case 1:
            Sbt_du->set_value(0.2097);
            Sbt_dv->set_value(0.105);
            Sbt_f->set_value(0.037);
            Sbt_k->set_value(0.06);
            break;
        case 2:
            Sbt_du->set_value(0.2097);
            Sbt_dv->set_value(0.105);
            Sbt_f->set_value(0.078);
            Sbt_k->set_value(0.061);
            break;
        case 3:
            Sbt_du->set_value(0.2097);
            Sbt_dv->set_value(0.105);
            Sbt_f->set_value(0.03);
            Sbt_k->set_value(0.062);
            break;
        case 4:
            Sbt_du->set_value(0.2097);
            Sbt_dv->set_value(0.105);
            Sbt_f->set_value(0.025);
            Sbt_k->set_value(0.06);
            break;
        case 5:
            Sbt_du->set_value(0.2097);
            Sbt_dv->set_value(0.105);
            Sbt_f->set_value(0.029);
            Sbt_k->set_value(0.057);
            break;
        case 6:
            Sbt_du->set_value(0.2097);
            Sbt_dv->set_value(0.105);
            Sbt_f->set_value(0.014);
            Sbt_k->set_value(0.054);
            break;
        case 7:
            Sbt_du->set_value(0.2097);
            Sbt_dv->set_value(0.105);
            Sbt_f->set_value(0.014);
            Sbt_k->set_value(0.045);
            break;
    }
    active_adj_event = true;
}

void GUI::bt_Clear_clicked() {
    solver->clear();
    update_widgets();
}

void GUI::glarea_resize(int, int) {
    adjust_size_entry();
    update_widgets();
}

void GUI::adjust_size_entry() {
    if(Swt_Size->get_state()){
        Etr_Width->set_sensitive(false);
        Etr_Height->set_sensitive(false);

        {
            std::lock_guard<std::mutex> lock(Etr_mutex);
            Etr_Width->set_text(std::to_string(Gl_con.getGlArea()->get_width()));
            Etr_Height->set_text(std::to_string(Gl_con.getGlArea()->get_height()));
        }

        solver->use_params(get_params());
    }
    else {
        Etr_Width->set_sensitive(true);
        Etr_Height->set_sensitive(true);
    }
}

void GUI::sw_clicked() {
    adjust_size_entry();
    update_widgets();
}

void GUI::update_widgets(){
    m_SolverWorker.has_stopped() ? Btn_PlayPause->set_image(*play) : Btn_PlayPause->set_image(*pause);
    buttonOverlay->change_play_status(!m_SolverWorker.has_stopped());

    Gl_con.getTexture()->set_info(solver->get_texture_info());
    Gl_con.getGlArea()->queue_render();
}

void GUI::bt_PlayPause_clicked() {

    if (m_SolverWorker.has_stopped()){
        if (m_WorkerThread) {
            std::cout << "Another worker thread is running" << std::endl;
        }
        else {
            m_WorkerThread = new std::thread([this] {m_SolverWorker.do_work(this);});
        }
    }
    else {
        m_SolverWorker.stop_work();
    }

    update_widgets();
}

void GUI::bt_Save_clicked() {
    if (screenshot_use_filename) {
        std::string res = spawn_save_dialog();
        if(!res.empty())
            solver->save_image(res);
    }
    else {
        solver->save_image("");
    }
}

bool GUI::on_key_press_or_release_event(GdkEventKey* event)
{
    if (event->type == GDK_KEY_PRESS){
        switch(event->keyval){
            case GDK_KEY_Escape:
            case GDK_KEY_q:
                Window->close();
                break;
            case GDK_KEY_s:
                screenshot_use_filename = false;
                Btn_Save->clicked();
                screenshot_use_filename = true;
                break;
            case GDK_KEY_f:
                Rev_Panel->get_reveal_child() ? set_panel_visible(false) : set_panel_visible(true);
                break;
            case GDK_KEY_g:
                buttonOverlay->get_reveal_child() ? buttonOverlay->set_reveal_child(false) : buttonOverlay->set_reveal_child(true);
                break;
            case GDK_KEY_F10:
                Window->is_maximized() ? Window->unmaximize() : Window->maximize();
                break;
            case GDK_KEY_F11:
                // Fullscreen state on signal : signal_window_state_event().connect
                // https://mail.gnome.org/archives/gtk-app-devel-list/2008-June/msg00074.html
                break;
            default:
                return false;
        }
        return true;
    }
    return false;
}

void GUI::notify()
{
    m_Dispatcher.emit();
}

void GUI::on_notification_from_worker_thread() {
    if (m_WorkerThread && m_SolverWorker.has_stopped())
    {
        if (m_WorkerThread->joinable())
            m_WorkerThread->join();
        delete m_WorkerThread;
        m_WorkerThread = nullptr;
    }
    update_widgets();
}

std::string GUI::spawn_save_dialog() {
    refBuilder->get_widget_derived("Save_Dialog", saveDialog, Window);
    saveDialog->set_current_folder(solver->get_image_folder());
    int result = saveDialog->run();

    std::string res;

    if(result == Gtk::RESPONSE_OK) {
        res = saveDialog->get_filename();
        std::cout << "File selected: " << res << std::endl;
    }

    saveDialog->hide();
    return res;
}
