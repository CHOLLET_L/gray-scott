#ifndef GRAY_SCOTT_GUI_H
#define GRAY_SCOTT_GUI_H
#include <string>
#include <thread>
#include <mutex>
#include <iostream>

#include <gtkmm-3.0/gtkmm.h>

#include "../Solver/Solver.h"
#include "../Common/params.h"
#include "SolverWorker.h"
#include "GlContext.h"
#include "ButtonOverlay.h"
#include "SaveDialog.h"

class GUI {
public:
    explicit GUI(int, char **, const std::string&, Solver*, const params& param);
    ~GUI();

    void run();

    template<typename Event> bool gl_mouse_event(Event);
    void event_product(coord);

    void glarea_resize(int, int);

    void notify();

    bool on_key_press_or_release_event(GdkEventKey*);

    void bt_PlayPause_clicked();
    void bt_Save_clicked();
    void bt_Clear_clicked();
    void bt_Panel_clicked();
    void bt_Overlay_clicked();

    void cob_preset_modified();

    void sw_clicked();

    params get_params();
    void set_params(const params&);

private:
    void init_with_file(const std::string&);

    void adjust_size_entry();

    void update_cob_preset();
    void adjust_preset_param();

    std::string spawn_save_dialog();

    void set_panel_visible(bool);

    void on_notification_from_worker_thread();

    void update_widgets();

    Solver* solver;
    Glib::Dispatcher m_Dispatcher;
    std::thread* m_WorkerThread = nullptr;
    SolverWorker m_SolverWorker;

    Glib::RefPtr<Gtk::Application> app;
    Glib::RefPtr<Gtk::Builder> refBuilder;

    GlContext Gl_con;

    std::mutex Etr_mutex;

    Gtk::Window*    Window = nullptr;
    Gtk::EventBox*  GlEvent = nullptr;
    Gtk::Overlay*   GlOverlay = nullptr;
    Gtk::Revealer*  Rev_Panel = nullptr;
    Gtk::Box*       Box_Panel = nullptr;


    Gtk::Entry*     Etr_Width = nullptr;
    Gtk::Entry*     Etr_Height = nullptr;
    Gtk::Switch*    Swt_Size = nullptr;

    Gtk::Switch*    Swt_Video = nullptr;
    Gtk::Entry*     Etr_filename = nullptr;

    Gtk::ComboBoxText*  CoB_method = nullptr;
    Gtk::Entry*         Etr_dt = nullptr;

    Gtk::ColorButton*    Cbt_fg = nullptr;
    Gtk::ColorButton*    Cbt_bg = nullptr;

    Gtk::ComboBoxText*  CoB_preset = nullptr;
    Gtk::SpinButton*    Sbt_du = nullptr;
    Gtk::SpinButton*    Sbt_dv = nullptr;
    Gtk::SpinButton*    Sbt_f = nullptr;
    Gtk::SpinButton*    Sbt_k = nullptr;

    Gtk::Button*    Btn_PlayPause = nullptr;
    Gtk::Button*    Btn_Save = nullptr;
    Gtk::Button*    Btn_Clear = nullptr;
    Gtk::Button*    Btn_End = nullptr;

    std::vector<Gtk::Widget*> widgets;

    ButtonOverlay*  buttonOverlay = nullptr;

    Glib::RefPtr<Gtk::Adjustment> Adj_du;
    Glib::RefPtr<Gtk::Adjustment> Adj_dv;
    Glib::RefPtr<Gtk::Adjustment> Adj_f;
    Glib::RefPtr<Gtk::Adjustment> Adj_k;

    bool active_adj_event = false;

    bool screenshot_use_filename = true;

    Gtk::Image* play = nullptr;
    Gtk::Image* pause = nullptr;

    SaveDialog* saveDialog = nullptr;
};

template<typename Event>
bool GUI::gl_mouse_event(Event ev){
    event_product(std::make_pair(ev->x, ev->y));
    return true;
}
#endif //GRAY_SCOTT_GUI_H
