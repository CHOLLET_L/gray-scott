#include "ButtonOverlay.h"

ButtonOverlay::ButtonOverlay(Gtk::Orientation orientation) : Gtk::Revealer(), MainBox(orientation){
    play = Gtk::Image(Gtk::IconTheme::get_default()->load_icon("media-playback-start",16));
    pause = Gtk::Image(Gtk::IconTheme::get_default()->load_icon("media-playback-pause",16));
    save = Gtk::Image(Gtk::IconTheme::get_default()->load_icon("media-floppy",16));
    first = Gtk::Image(Gtk::IconTheme::get_default()->load_icon("go-first",16));

    PlayPause = new Gtk::Button();
    Save = new Gtk::Button();
    DisplayPanel = new Gtk::Button();

    PlayPause->add(play);
    Save->add(save);
    DisplayPanel->add(first);

    MainBox.pack_start(*PlayPause);
    MainBox.pack_start(*Save);
    MainBox.pack_start(*DisplayPanel);
    set_halign(Gtk::ALIGN_END);
    set_valign(Gtk::ALIGN_END);

    add(MainBox);

//    set_transition_type(Gtk::REVEALER_TRANSITION_TYPE_NONE);
//    set_reveal_child(true);
    set_transition_type(Gtk::REVEALER_TRANSITION_TYPE_SLIDE_UP);

    show_all_children();
//    set_visible(false);
}

Gtk::Button* ButtonOverlay::getPlay() const {
    return PlayPause;
}
Gtk::Button* ButtonOverlay::getSave() const{
    return Save;
}
Gtk::Button* ButtonOverlay::getDisplayPanel() const{
    return DisplayPanel;
}

ButtonOverlay::~ButtonOverlay() {
    delete PlayPause;
    delete Save;
    delete DisplayPanel;
}

void ButtonOverlay::change_play_status(bool playing) {
    playing ? PlayPause->set_image(pause) : PlayPause->set_image(play);
}