#ifndef GRAY_SCOTT_GlCONTEXT_H
#define GRAY_SCOTT_GlCONTEXT_H
#include <iostream>

#include <gtkmm-3.0/gtkmm.h>

#include <GL/glew.h>

#include "../Common/Texture.h"

class GlContext {
public:
    GlContext() = default;
    ~GlContext() = default;

    Gtk::GLArea *getGlArea() const;
    void setGlArea(Gtk::GLArea *glArea);

    void initialize();

    void realize();
    void unrealize();
    bool render(const Glib::RefPtr<Gdk::GLContext>& context);

    Texture* getTexture();

private:
    Gtk::GLArea* GlArea = nullptr;

    GLuint Vao_id = 0;
    GLuint Buffer_id = 0;
    GLuint Program_id = 0;

    GLuint Tex_id = 0;
    Texture Tex;
};

#endif //GRAY_SCOTT_GlCONTEXT_H
