#ifndef GRAY_SCOTT_BUTTONOVERLAY_H
#define GRAY_SCOTT_BUTTONOVERLAY_H

#include <gtkmm-3.0/gtkmm.h>

class ButtonOverlay : public Gtk::Revealer{
public:
    explicit ButtonOverlay(Gtk::Orientation);
    ~ButtonOverlay() override;

    void change_play_status(bool);

    Gtk::Button* getPlay() const;
    Gtk::Button* getSave() const;
    Gtk::Button* getDisplayPanel() const;

private:
    Gtk::Box MainBox;

    Gtk::Button* PlayPause = nullptr;
    Gtk::Button* Save = nullptr;
    Gtk::Button* DisplayPanel = nullptr;

    Gtk::Image play;
    Gtk::Image pause;
    Gtk::Image save;
    Gtk::Image first;
};


#endif //GRAY_SCOTT_BUTTONOVERLAY_H
