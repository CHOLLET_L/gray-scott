#include "GUI.h"

GUI::GUI(int argc, char **argv, const std::string& filename, Solver* sol, const params& param) : solver(sol), m_SolverWorker(sol) {
    app = Gtk::Application::create(argc, argv, "Gray.Scott");
    refBuilder = Gtk::Builder::create();

    buttonOverlay = new ButtonOverlay(Gtk::ORIENTATION_HORIZONTAL);

    play = new Gtk::Image(Gtk::IconTheme::get_default()->load_icon("media-playback-start",16));
    pause = new Gtk::Image(Gtk::IconTheme::get_default()->load_icon("media-playback-pause",16));

    init_with_file(filename);

    m_Dispatcher.connect(sigc::mem_fun(*this, &GUI::on_notification_from_worker_thread));

    set_params(param);
    Gl_con.getTexture()->set_info(solver->get_texture_info());

    active_adj_event = true;

    Window->set_double_buffered(false);
    // Stop flickering on Xorg
    // Don't start with Wayland
    
    widgets = {Window, GlEvent, GlOverlay, Rev_Panel, Box_Panel, Etr_Width, Etr_Height, Swt_Size, Swt_Video, Etr_filename, CoB_method, Etr_dt, Cbt_fg, Cbt_bg, CoB_preset, Sbt_du, Sbt_dv, Sbt_f, Sbt_k, Btn_PlayPause, Btn_Save, Btn_Clear, Btn_End};
}

GUI::~GUI() {
    if (m_WorkerThread && m_WorkerThread->joinable()) {
        m_SolverWorker.stop_work();
        m_WorkerThread->join();
    }

    delete buttonOverlay;

    delete play;
    delete pause;

    for (auto& widget : widgets){
        delete widget;
    }
}


void GUI::init_with_file(const std::string& filename) {
    try
    {
        refBuilder->add_from_file(filename);
    }
    catch(const Glib::FileError& ex)
    {
        std::cerr << "FileError: " << ex.what() << std::endl;
    }
    catch(const Glib::MarkupError& ex)
    {
        std::cerr << "MarkupError: " << ex.what() << std::endl;
    }
    catch(const Gtk::BuilderError& ex)
    {
        std::cerr << "BuilderError: " << ex.what() << std::endl;
    }

    refBuilder->get_widget("Main_Window", Window);
    if(Window){
        Window->signal_key_press_event().connect(sigc::mem_fun(*this, &GUI::on_key_press_or_release_event));
        Window->signal_key_release_event().connect(sigc::mem_fun(*this, &GUI::on_key_press_or_release_event));
        Window->add_events(Gdk::KEY_PRESS_MASK | Gdk::KEY_RELEASE_MASK);

        refBuilder->get_widget("GlEventBox", GlEvent);
        refBuilder->get_widget("Overlay_GL", GlOverlay);
        refBuilder->get_widget("Rev_panel", Rev_Panel);
        refBuilder->get_widget("Box_panel", Box_Panel);

        refBuilder->get_widget("Etr_s_h", Etr_Width);
        refBuilder->get_widget("Etr_s_v", Etr_Height);
        refBuilder->get_widget("Swi_s", Swt_Size);

        refBuilder->get_widget("Swi_v", Swt_Video);
        refBuilder->get_widget("Etr_f_name", Etr_filename);

        refBuilder->get_widget("CoB_method", CoB_method);
        refBuilder->get_widget("Etr_deltat", Etr_dt);

        refBuilder->get_widget("Cbt_bg", Cbt_bg);
        refBuilder->get_widget("Cbt_fg", Cbt_fg);

        refBuilder->get_widget("CoB_preset", CoB_preset);
        refBuilder->get_widget("Sbt_Du", Sbt_du);
        refBuilder->get_widget("Sbt_Dv", Sbt_dv);
        refBuilder->get_widget("Sbt_f", Sbt_f);
        refBuilder->get_widget("Sbt_k", Sbt_k);

        refBuilder->get_widget("Btn_Play", Btn_PlayPause);
        refBuilder->get_widget("Btn_Save", Btn_Save);
        refBuilder->get_widget("Btn_Clear", Btn_Clear);
        refBuilder->get_widget("Btn_End", Btn_End);

        Adj_du = Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(refBuilder->get_object("Adj_du"));
        Adj_dv = Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(refBuilder->get_object("Adj_dv"));
        Adj_f = Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(refBuilder->get_object("Adj_f"));
        Adj_k = Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(refBuilder->get_object("Adj_k"));


        Gtk::GLArea* tmp;
        refBuilder->get_widget("GlSpace", tmp);
        Gl_con.setGlArea(tmp);
        Gl_con.initialize();

        for (auto& widget : widgets){
            if (!widget){
                std::cerr << "ERREUR : Un des Widgets n'a pus être chargés." << std::endl;
                exit(1);
            }
        }

        Rev_Panel->set_transition_type(Gtk::REVEALER_TRANSITION_TYPE_SLIDE_RIGHT);
        Rev_Panel->set_transition_duration(500);

        GlOverlay->add_overlay(*buttonOverlay);


        Swt_Size->property_active().signal_changed().connect(sigc::mem_fun(*this, &GUI::sw_clicked));
        Gl_con.getGlArea()->signal_resize().connect(sigc::mem_fun(*this, &GUI::glarea_resize));

//        Rev_Panel->signal_size_allocate().connect(sigc::mem_fun(*this, &GUI::test));
//        Box_Panel->signal_size_allocate().connect(sigc::mem_fun(*this, &GUI::test));

        buttonOverlay->getPlay()->signal_clicked().connect(sigc::mem_fun(*this, &GUI::bt_PlayPause_clicked));
        buttonOverlay->getSave()->signal_clicked().connect(sigc::mem_fun(*this, &GUI::bt_Save_clicked));
        buttonOverlay->getDisplayPanel()->signal_clicked().connect(sigc::mem_fun(*this, &GUI::bt_Overlay_clicked));

        Btn_PlayPause->signal_clicked().connect(sigc::mem_fun(*this, &GUI::bt_PlayPause_clicked));
        Btn_Save->signal_clicked().connect(sigc::mem_fun(*this, &GUI::bt_Save_clicked));
        Btn_Clear->signal_clicked().connect(sigc::mem_fun(*this, &GUI::bt_Clear_clicked));
        Btn_End->signal_clicked().connect(sigc::mem_fun(*this, &GUI::bt_Panel_clicked));

        CoB_preset->signal_changed().connect(sigc::mem_fun(*this, &GUI::cob_preset_modified));

        Adj_du->signal_value_changed().connect(sigc::mem_fun(*this, &GUI::update_cob_preset));
        Adj_dv->signal_value_changed().connect(sigc::mem_fun(*this, &GUI::update_cob_preset));
        Adj_f->signal_value_changed().connect(sigc::mem_fun(*this, &GUI::update_cob_preset));
        Adj_k->signal_value_changed().connect(sigc::mem_fun(*this, &GUI::update_cob_preset));

        Btn_PlayPause->set_image(*play);

        GlEvent->add_events(Gdk::BUTTON1_MOTION_MASK | Gdk::BUTTON_PRESS_MASK);
        GlEvent->signal_button_press_event().connect(sigc::mem_fun(*this, &GUI::gl_mouse_event<GdkEventButton*>));
        GlEvent->signal_motion_notify_event().connect(sigc::mem_fun(*this, &GUI::gl_mouse_event<GdkEventMotion*>));

        Window->show_all_children();
    }
}

void GUI::run(){
    app->run(*Window);
}

params GUI::get_params(){
    std::lock_guard<std::mutex> lock(Etr_mutex);

    params data;

    data.width = std::stoi(Etr_Width->get_text());
    data.height = std::stoi(Etr_Height->get_text());

    data.methode = Method(CoB_method->get_active_row_number());
//    data.dt = std::stod(Etr_dt->get_text());

    data.filename = Etr_filename->get_text();
    data.create_video = Swt_Video->get_state();

    Gdk::RGBA bg = Cbt_bg->get_rgba();
    Gdk::RGBA fg = Cbt_fg->get_rgba();

    data.background = rgba_to_color(bg);
    data.foreground = rgba_to_color(fg);

    data.du = Sbt_du->get_value();
    data.dv = Sbt_dv->get_value();
    data.feed = Sbt_f->get_value();
    data.kill = Sbt_k->get_value();

    return data;
}

void GUI::set_params(const params& param){
    std::lock_guard<std::mutex> lock(Etr_mutex);

    Etr_Width->set_text(std::to_string(param.width));
    Etr_Height->set_text(std::to_string(param.height));

    CoB_method->set_active(int(param.methode));
//    Etr_dt->set_text(std::to_string(param.dt));

    Etr_filename->set_text(param.filename);
    Swt_Video->set_state(param.create_video);

    Cbt_bg->set_rgba(color_to_rgba(param.background));
    Cbt_fg->set_rgba(color_to_rgba(param.foreground));

    Sbt_du->set_value(param.du);
    Sbt_dv->set_value(param.dv);
    Sbt_f->set_value(param.feed);
    Sbt_k->set_value(param.kill);
}
