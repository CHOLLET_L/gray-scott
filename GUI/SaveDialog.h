#ifndef GRAY_SCOTT_SAVEDIALOG_H
#define GRAY_SCOTT_SAVEDIALOG_H

#pragma GCC diagnostic push

#pragma GCC diagnostic ignored "-Wold-style-cast"
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wredundant-decls"
#pragma GCC diagnostic ignored "-Wuseless-cast"
#pragma GCC diagnostic ignored "-Wctor-dtor-privacy"

// Usage of the C-struct BaseObjectType cause the inlining of C-style code and triggers warnings
#include <gtkmm.h>

#pragma GCC diagnostic pop

class SaveDialog : public Gtk::FileChooserDialog {
public:
    SaveDialog(BaseObjectType * cobject, Glib::RefPtr<Gtk::Builder>  builder, Gtk::Window*);
    SaveDialog(const SaveDialog &) = delete;
    SaveDialog &operator=(const SaveDialog &) = delete;
    ~SaveDialog() override = default;

    std::string get_filename();

    Glib::RefPtr<Gtk::Builder> refBuilder;

    Glib::RefPtr<Gtk::FileFilter>   Ftr_all;

    Glib::RefPtr<Gtk::FileFilter>   Ftr_Img_png;
    Glib::RefPtr<Gtk::FileFilter>   Ftr_Img_jpg;
    Glib::RefPtr<Gtk::FileFilter>   Ftr_Img_ppm;
};


#endif //GRAY_SCOTT_SAVEDIALOG_H
