#ifndef GRAY_SCOTT_SOLVERWORKER_H
#define GRAY_SCOTT_SOLVERWORKER_H

#include "../Solver/Solver.h"

class GUI;

class SolverWorker {
public:
    explicit SolverWorker(Solver*);
    ~SolverWorker() = default;

    void do_work(GUI* caller);

    void stop_work();
    bool has_stopped() const;

private:
    mutable std::mutex m_Mutex;

    Solver* m_Solver;
    bool m_shall_stop = false;
    bool m_has_stopped = true;

};


#endif //GRAY_SCOTT_SOLVERWORKER_H
