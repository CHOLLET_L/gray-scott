#include <iostream>
#include <thread>

#include "GUI.h"
#include "SolverWorker.h"

SolverWorker::SolverWorker(Solver* solver) : m_Mutex(), m_Solver(solver) {}

void SolverWorker::stop_work()
{
    std::lock_guard<std::mutex> lock(m_Mutex);
    m_shall_stop = true;
}

bool SolverWorker::has_stopped() const
{
    std::lock_guard<std::mutex> lock(m_Mutex);
    return m_has_stopped;
}

void SolverWorker::do_work(GUI* caller)
{
    {
        std::lock_guard<std::mutex> lock(m_Mutex);
        m_has_stopped = false;
    }

    for (int i = 0; i < 100000; ++i)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(2));
        {
            std::lock_guard<std::mutex> lock(m_Mutex);

            m_Solver->use_params(caller->get_params());
            m_Solver->solve();

            if (m_shall_stop)
            {
                break;
            }
        }

        caller->notify();
    }

    {
        std::lock_guard<std::mutex> lock(m_Mutex);
        m_shall_stop = false;
        m_has_stopped = true;
    }

    caller->notify();
}