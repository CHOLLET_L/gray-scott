#include "GlContext.h"

static const GLfloat vertex_data[] = {
        -1.f,   1.f,  0.f,
        -1.f,  -1.f,  0.f,
        1.f,   -1.f,  0.f,

        -1.f,   1.f,  0.f,
        1.f,   -1.f,  0.f,
        1.f,   1.f,   0.f,
};

std::string vertex_src = R"(
#version 330 core
layout (location = 0) in vec3 aPos;
out vec2 vertexPos;

void main()
{
    gl_Position = vec4(aPos, 1.0);
    vertexPos = .5*(vec2(gl_Position.x, -gl_Position.y) + vec2(1., 1.));
}
)";

std::string fragment_src = R"(
#version 330 core
out vec4 FragColor;
in vec2 vertexPos;

uniform sampler2D Texture;

void main()
{
    float c = texture(Texture, vertexPos).r;
    FragColor = vec4(c, c, c, 1.);
//    FragColor = vec4(vertexPos, 0, 1);
}
)";
static const char *vertex_str = vertex_src.c_str();
static const char *fragment_str = fragment_src.c_str();

Gtk::GLArea *GlContext::getGlArea() const {
    return GlArea;
}

void GlContext::setGlArea(Gtk::GLArea *glArea) {
    GlArea = glArea;
}

Texture* GlContext::getTexture(){
    return &Tex;
}

void GlContext::initialize(){
    GlArea->signal_realize().connect(sigc::mem_fun(*this, &GlContext::realize));
    GlArea->signal_unrealize().connect(sigc::mem_fun(*this, &GlContext::unrealize), false);
    GlArea->signal_render().connect(sigc::mem_fun(*this, &GlContext::render), false);

    GlArea->set_required_version(3, 3);
    GlArea->set_auto_render(true);
}

void GlContext::realize() {
    GlArea->make_current();

    GLenum return_code = glewInit();
    if (return_code) {
        std::cerr << "Error while initializing GLEW: " << glewGetErrorString(return_code) << std::endl;
//        exit(-1);
    }

    glClearColor(1, 1, 0.8, 1.0);

    glGenVertexArrays(1, &Vao_id);
    glBindVertexArray(Vao_id);

    glGenBuffers(1, &Buffer_id);
    glBindBuffer(GL_ARRAY_BUFFER, Buffer_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    GLuint vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vertex_str, nullptr);
    glCompileShader(vertex);

    int status;
    glGetShaderiv(vertex, GL_COMPILE_STATUS, &status);
    if(status == GL_FALSE)
    {
        int log_len;
        glGetShaderiv(vertex, GL_INFO_LOG_LENGTH, &log_len);

        std::string log_space(static_cast<long unsigned int>(log_len+1), ' ');
        glGetShaderInfoLog(vertex, log_len, nullptr, const_cast<GLchar*>(log_space.c_str()));

        std::cerr << "Compile failure in " <<
             "vertex shader: " << log_space << std::endl;

        glDeleteShader(vertex);
    }
    
    GLuint fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fragment_str, nullptr);
    glCompileShader(fragment);

    glGetShaderiv(fragment, GL_COMPILE_STATUS, &status);
    if(status == GL_FALSE)
    {
        int log_len;
        glGetShaderiv(fragment, GL_INFO_LOG_LENGTH, &log_len);

        std::string log_space(static_cast<long unsigned int>(log_len+1), ' ');
        glGetShaderInfoLog(fragment, log_len, nullptr, const_cast<GLchar*>(log_space.c_str()));

        std::cerr << "Compile failure in " <<
                  "fragment shader: " << log_space << std::endl;

        glDeleteShader(fragment);
    }

    Program_id = glCreateProgram();
    glAttachShader(Program_id, vertex);
    glAttachShader(Program_id, fragment);

    glLinkProgram(Program_id);

    glGetProgramiv(Program_id, GL_LINK_STATUS, &status);
    if(status == GL_FALSE)
    {
        int log_len;
        glGetProgramiv(Program_id, GL_INFO_LOG_LENGTH, &log_len);

        std::string log_space(static_cast<long unsigned int>(log_len+1), ' ');
        glGetProgramInfoLog(Program_id, log_len, nullptr, const_cast<GLchar*>(log_space.c_str()));

        std::cerr << "Linking failure: " << log_space << std::endl;

        glDeleteProgram(Program_id);
        Program_id = 0;
    }
    
    glDeleteShader(vertex);
    glDeleteShader(fragment);

    glGenTextures(1, &Tex_id);

    Tex.bind(Tex_id);
//    glUniform1i(glGetUniformLocation(Program_id, "Texture"), 0); // Texture slot 0
}

void GlContext::unrealize() {
    glDeleteBuffers(1, &Buffer_id);
    glDeleteProgram(Program_id);
}

bool GlContext::render(const Glib::RefPtr<Gdk::GLContext> &) {
    if(Tex.get_updated()){
        Tex.bind(Tex_id);
        Tex.set_updated(false);
    }

    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(Program_id);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Tex_id);
    glUniform1i(glGetUniformLocation(Program_id, "Texture"), 0);

    glBindBuffer(GL_ARRAY_BUFFER, Buffer_id);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    glDrawArrays(GL_TRIANGLES, 0, 6);

    glDisableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);

    return true;
}
