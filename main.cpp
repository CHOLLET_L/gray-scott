#include <iostream>

#include "Solver/Solver.h"
#include "GUI/GUI.h"

int main(int argc, char **argv) {

    if (argc > 1) {
        if (std::string(argv[1]) == "-no-gui") {
            params par = {3,
                          3,
                          Method::FTCS,
                          0.001,
                          0.2097,
                          0.105,
                          0.03,
                          0.063,
                          {1.0, 1.0, 1.0, 1.0},
                          {0.0, 0.0, 0.0, 1.0},
                          false,
                          "File"};

            Solver solver(par);

            for (int _=0; _<10000; ++_) {
                solver.solve();
                if (_%100 == 0) {
                    std::cout << "T\n";
                }
            }
            solver.solve();
            solver.solve();
            solver.use_params(par);

        }
    }
    else {
        params para;
        Solver solver(para);

        GUI gui = GUI(argc, argv, "../GS.glade", &solver, para);
        gui.run();

    }

    return 0;
}
