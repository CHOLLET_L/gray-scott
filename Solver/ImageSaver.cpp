#include <opencv2/opencv.hpp>
#include "ImageSaver.h"

ImageSaver::ImageSaver(const std::filesystem::path& root) {
    std::filesystem::path tmp (root / image_folder);
    if (!std::filesystem::exists(tmp))
        std::filesystem::create_directory(tmp);
    final_path = tmp;
    std::cout << final_path << std::endl;
}

void ImageSaver::load_image(Image img) {
    std::lock_guard<std::mutex> lock(img_buffer_lock);

    if (img.filename.empty())
        img.filename = get_proper_name();

    image_buffer.push_back(std::move(img));
}

void ImageSaver::save_images() {
    std::lock_guard<std::mutex> lock(img_buffer_lock);

//        size = image_buffer.size();
//        if (size == 0) return;

    Image img = std::move(image_buffer.back());
    image_buffer.pop_back();

    cv::Mat mat(int(img.height), int(img.width), CV_32FC1, img.matrix.data());

    cv::Mat mat_converted;

    mat.convertTo(mat_converted, CV_8UC1, 255.0);

    std::cout << "Image Saved to: " << img.filename << std::endl;

    cv::imwrite(img.filename, mat_converted);

}

std::string ImageSaver::get_proper_name(){
    int max = 0;
    for (const auto & file : std::filesystem::directory_iterator(final_path)) {
        if (!file.path().has_extension())
            continue;

        std::string name(file.path().filename());

        if (name.size() < image_base.length() || std::string(name.begin(), name.begin() + long(image_base.length())) != image_base)
            continue;

        unsigned int ext_lenght = std::string(file.path().extension()).size();
        unsigned int i = 0;
        std::string nb;
        while(isdigit(name.at(name.size()-ext_lenght - 1 - i))){
            nb = name.at(name.size()-ext_lenght - 1 - i) + nb;
            ++i;
        }
        int nb_i = std::stoi(nb);
        if (nb_i > max) max = nb_i;
    }
    return final_path.string() + image_base + std::to_string(max + 1) + ".png";
}

std::filesystem::path ImageSaver::get_image_folder() const{
    return final_path;
}
