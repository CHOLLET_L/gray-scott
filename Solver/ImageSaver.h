#ifndef GRAY_SCOTT_IMAGESAVER_H
#define GRAY_SCOTT_IMAGESAVER_H

#include <string>
#include <map>
#include <vector>
#include <mutex>

#include <filesystem>

#include <iostream>

struct Image {
    std::string filename;
    long unsigned int width;
    long unsigned int height;
    std::vector<float> matrix;
};

class ImageSaver {
public:
    explicit ImageSaver(const std::filesystem::path&);
    ImageSaver(const ImageSaver &) = delete;
    ImageSaver &operator=(const ImageSaver &) = delete;
    ~ImageSaver() = default;

    std::filesystem::path get_image_folder() const;

    void load_image(Image);
    void save_images();
private:
    std::string get_proper_name();
    std::filesystem::path final_path;
    std::filesystem::path image_folder= "Images/";

    std::string image_base = "gray-scott_";

    std::mutex img_buffer_lock;
    std::vector<Image> image_buffer;
};


#endif //GRAY_SCOTT_IMAGESAVER_H
