#include "Solver.h"

void Solver::ftcs(std::vector<double>& domain_u_new, std::vector<double>& domain_v_new) {

    #pragma omp parallel for default(none) shared(domain_u_new, domain_v_new)
    for (vectype i = 0; i < height; ++i){
        bool i_flag = i == 0 || i == (height - 1);
        for (vectype j = 0; j < width; ++j) {
            vectype pos = elem_pos(i, j);

            if ( i_flag || j == 0 || j == (width -1)) {
                domain_u_new[pos] = .78;
                domain_v_new[pos] = 0.0;
                continue;
            }

            double tmp = domain_v[pos] * domain_v[pos] * domain_u[pos];

            domain_u_new[pos] =
                    domain_u[pos] +
//                    + domain_u_old[pos]
//                    + 2. *
                      d_t * (
                              local_param.du * (second_derivative(i, j, domain_u, true) + second_derivative(i, j, domain_u, false))
                              - tmp
                              + local_param.feed * (1 - domain_u[pos])
                      )
                    ;

            domain_v_new[pos] =
                    domain_v[pos] +
//                    + domain_v_old[pos]
//                    + 2. *
                      d_t * (
                              local_param.dv * (second_derivative(i, j, domain_v, true) + second_derivative(i, j, domain_v, false))
                              + tmp
                              - domain_v[pos] * (local_param.feed + local_param.kill)
                      )
                    ;

        }
    }
}
