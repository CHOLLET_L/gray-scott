#ifndef GRAY_SCOTT_SOLVER_H
#define GRAY_SCOTT_SOLVER_H

#include <iostream>
#include <vector>
#include <utility>

#include "ImageSaver.h"

#include "../Common/Texture.h"
#include "../Common/params.h"

using vectype = long unsigned int;

class Solver {
public:
    explicit Solver(const params&);
    ~Solver() = default;

    void use_params(const params&);

    void solve();

    void add_product(coord);
    void clear();

    void save_image(std::string);

    TexInfo get_texture_info();

    std::filesystem::path get_image_folder() const;

private:
    void initialize_domain();
    void set_deltas();

    std::filesystem::path root_path;

    vectype elem_pos(vectype, vectype) const;
    double second_derivative(vectype, vectype, std::vector<double>&, bool) const;

    void ftcs(std::vector<double>&, std::vector<double>&);
    void btcs(std::vector<double>&, std::vector<double>&);
    void crank_nicolson(std::vector<double>&, std::vector<double>&);

    void apply_added_product();

    std::mutex domain_lock;
    params local_param;

    void update_size();
    void update_vector(vectype, vectype, std::vector<double>&);

    bool updated = false;

    vectype width;
    vectype height;

    std::vector<double> domain_u;
    std::vector<double> domain_u_old;

    std::vector<double> domain_v;
    std::vector<double> domain_v_old;
    std::vector<float> domain_v_float;

    double d_t = 0;
    double d_x = 0;
    double d_y = 0;

    double add_value;

    std::mutex add_lock;
    std::vector<coord> add_calc;

    ImageSaver imageSaver;
};

#endif //GRAY_SCOTT_SOLVER_H
