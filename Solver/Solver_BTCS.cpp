#include "Solver.h"

void Solver::btcs(std::vector<double>& domain_u_new, std::vector<double>& domain_v_new){

    for (vectype i = 0; i < height; ++i){
        bool i_flag = ( i <= 1 || i == (height - 1));
        for (vectype j = 0; j < width; ++j) {
            if ( i_flag || j == 0 || j == (width -1)) {
                domain_u_new[elem_pos(i, j)] = 1.0;
                domain_v_new[elem_pos(i, j)] = 0.0;
                continue;
            }

            double tmp = domain_u_new[elem_pos(i - 1, j)] * domain_v_new[elem_pos(i - 1, j)] * domain_v_new[elem_pos(i - 1, j)];

            domain_u_new[elem_pos(i, j)] =
                    - domain_u_new[elem_pos(i - 2, j)]
                    + 2. * domain_u_new[elem_pos(i - 1, j)]
                    + d_y * d_y * (
                            - second_derivative(i - 1, j, domain_u_new, true)
                            + (1 / local_param.du) * (
                                    + tmp
                                    - local_param.feed * (1. - domain_u_new[elem_pos(i - 1, j)])
                                    + ((domain_u_new[elem_pos(i - 1, j)] - domain_u_old[elem_pos(i - 1, j)]) / (2. * d_t))
                            )

                    )
                    ;

            domain_v_new[elem_pos(i, j)] =
                    - domain_v_new[elem_pos(i - 2, j)]
                    + 2. * domain_v_new[elem_pos(i - 1, j)]
                    + d_y * d_y * (
                            - second_derivative(i - 1, j, domain_v_new, true)
                            + (1 / local_param.dv) * (
                                    - tmp
                                    + (local_param.feed + local_param.kill) * domain_v_new[elem_pos(i - 1, j)]
                                    + ((domain_v_new[elem_pos(i - 1, j)] - domain_v_old[elem_pos(i - 1, j)]) / (2. * d_t))
                            )

                    )
                    ;
        }
    }

}