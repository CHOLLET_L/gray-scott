#include "../Common/Get_Home.h"
#include "Solver.h"

Solver::Solver(const params& para) : root_path(get_home()), imageSaver(root_path){
    width = vectype(para.width);
    height = vectype(para.height);

    use_params(para);

    add_value = .9;

    initialize_domain();

    set_deltas();
}

void Solver::initialize_domain() {
    domain_u = std::vector<double>(width*height, 1.);
    domain_u_old = domain_u;

    domain_v = std::vector<double>(width*height, 0.);

//    domain_v[10 + 10*width] = .6;
//    domain_v[4] = add_value;
//    domain_v[10 + 10*width] = .001;
//    domain_v[10 + 9*width] = .2;

    domain_v_old = domain_v;
}

void Solver::use_params(const params& param) {
    local_param = param;

    if (vectype(local_param.width) != width
        || vectype(local_param.height) != height) update_size();
}

double Solver::second_derivative(const vectype i, const vectype j, std::vector<double>& mat, bool along_x) const {
    double result = 0;
    if (along_x) {
        // Along x axis
        if (j != 0) {
            result += mat[elem_pos(i, j - 1)];
        }
        if (j != (width - 1)) {
            result += mat[elem_pos(i, j + 1)];
        }

        result -= 2 * mat[elem_pos(i, j)];
        result /= d_x * d_x;
    }
    else {
        // Along y axis
        if (i != 0) {
            result += mat[elem_pos(i - 1, j)];
        }
        if (i != (height - 1)) {
            result += mat[elem_pos(i + 1, j)];
        }

        result -= 2 * mat[elem_pos(i, j)];
        result /= d_y * d_y;
    }

    return result;
}

void Solver::solve() {
//    std::cout << "\r" << domain_v[10 + 10*width] << std::flush;
//    std::cout << std::to_string(*std::max_element(domain_v.begin(), domain_v.end())) << std::endl;
    apply_added_product();

    std::lock_guard<std::mutex> lock(domain_lock);

    vectype n = domain_u.size();

    std::vector<double> domain_u_new(n);
    std::vector<double> domain_v_new(n);

    switch (local_param.methode){
        case Method::FTCS:
        default:
            ftcs(domain_u_new, domain_v_new);
            break;
        case Method::BTCS:
            btcs(domain_u_new, domain_v_new);
            break;
        case Method::CRANK_NICOLSON:
            crank_nicolson(domain_u_new, domain_v_new);
            break;
    }

    domain_u.swap(domain_u_old);
    domain_u_new.swap(domain_u);

    domain_v.swap(domain_v_old);
    domain_v_new.swap(domain_v);

    updated = true;
}

void Solver::update_vector(const vectype new_width, const vectype new_height, std::vector<double>& domain){
    std::vector<double> new_domain(new_width * new_height);

    double width_ration = double(width) / double(new_width);
    double height_ration = double(height) / double(new_height);

    for (vectype i = 0; i < new_height; ++i){
        auto tr_i = vectype(double(i) * height_ration);
        for (vectype j = 0; j < new_width; ++j){
            auto tr_j = vectype(double(j) * width_ration);
            new_domain[i * new_width + j] = domain[elem_pos(tr_i, tr_j)];
            // i * new_width + j -> elem_pos with new dimensions
        }
    }

    domain.swap(new_domain);
}

void Solver::update_size() {
    std::lock_guard<std::mutex> lock(domain_lock);

    auto lp_w = vectype(local_param.width);
    auto lp_h = vectype(local_param.height);

    update_vector(lp_w, lp_h, domain_u);
    update_vector(lp_w, lp_h, domain_u_old);
    update_vector(lp_w, lp_h, domain_v);
    update_vector(lp_w, lp_h, domain_v_old);

    width = lp_w;
    height = lp_h;

    set_deltas();

    updated = true;
}

void Solver::save_image(std::string filename) {
    domain_v_float = std::vector<float>(domain_v.begin(), domain_v.end());

    imageSaver.load_image({std::move(filename), width, height, domain_v_float});
    imageSaver.save_images();
}

TexInfo Solver::get_texture_info() {
    apply_added_product();

    domain_v_float = std::vector<float>(domain_v.begin(), domain_v.end());

    if (updated) {
        updated = false;
        return TexInfo{true, int(width), int(height), &domain_v_float};
    }
    else {
        return TexInfo{false, int(width), int(height), &domain_v_float};
    }

}

void Solver::set_deltas() {
//    d_x = 1. / double((width - 1));
//    d_y = 1. / double((height - 1));
//    d_t = (d_y*d_x / (d_x + d_y)) / 400;
//    d_t = 0.1 * (d_x*d_x / 4.);
    //    CFL : 4 * d_t / d_x^2 < 1

    d_x = 1;
    d_y = 1;
    d_t = .8;

}

vectype Solver::elem_pos(const vectype i, const vectype j) const{
    return i * width + j;
}

void Solver::add_product(const coord c) {
    std::lock_guard<std::mutex> lock(add_lock);

    // Check for out of bound coordinates
    if (c.second < 0 || c.second >= 1 || c.first < 0 || c.first >= 1) return;

    // Conversion from screen space to matrice coordinate
    add_calc.emplace_back(c.second, c.first);
}

void Solver::apply_added_product() {
    std::lock_guard<std::mutex> lock_2(domain_lock);
    std::lock_guard<std::mutex> lock_1(add_lock);

    for (auto & couple: add_calc){
        domain_v[elem_pos(couple.first * height, couple.second * width)] = add_value;
        domain_v_old[elem_pos(couple.first * height, couple.second * width)] = add_value;
    }
    if (!add_calc.empty()){
        updated = true;
        add_calc.clear();
    }
}

void Solver::clear(){
    std::lock_guard<std::mutex> lock_2(domain_lock);
    std::lock_guard<std::mutex> lock_1(add_lock);

    initialize_domain();

    add_calc.clear();
    updated = true;
}

std::filesystem::path Solver::get_image_folder() const {
    return imageSaver.get_image_folder();
}
