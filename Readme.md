# Gray-Scott

Gray-Scott is a program intended to simulate chemical reactions.
The main purpose is to produce animation and not a scientific simulation.

### Dependencies

+ Gtkmm
+ OpenGL
+ OpenCV


### TO-DO
#### General
+ Add color
+ Draw with a brush
#### Maths
+ Simulation time management
+ Add numerical methods
#### Interface
+ Video saver
+ Configuration saver
+ Adjust slider precision
#### Code improvement
+ Canonic form for classes
+ Check proper usage of `std::move` etc ...
+ Usage of smart pointers
+ Check containers type