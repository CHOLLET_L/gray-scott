#ifndef GRAY_SCOTT_GET_HOME_H
#define GRAY_SCOTT_GET_HOME_H

#include <cstdlib>

#include <filesystem>
#include <string>

inline std::filesystem::path get_home(){
    std::filesystem::path home = std::filesystem::current_path();
#ifdef unix
    home = getenv("HOME");
#elif defined(_WIN32)
    home = getenv("HOMEDRIVE") / getenv("HOMEPATH");
#endif

#ifdef DEBUG_OUTPUT_DIR
    home = DEBUG_OUTPUT_DIR;
#endif

    return home;
}

#endif //GRAY_SCOTT_GET_HOME_H
