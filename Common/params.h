#ifndef GRAY_SCOTT_PARAMS_H
#define GRAY_SCOTT_PARAMS_H
#include <string>

#include <gtkmm-3.0/gtkmm.h>

using coord = std::pair<double, double>;

enum class Method{
    FTCS,
    BTCS,
    CRANK_NICOLSON
};

struct color{
    double r = 0.0;
    double g = 0.0;
    double b = 0.0;
    double a = 1.0;
};

struct params {
    int width = 300;
    int height = 300;

    Method methode = Method::FTCS;
    double dt = 0.001;

//    double du = 0.01;
//    double dv = 0.005;

//    double du = 0.4;
//    double dv = 0.19;

    double du = 0.2097;
    double dv = 0.105;
    double feed = 0.037;
    double kill = 0.06;

    color background = {1.0, 1.0, 1.0, 1.0};
    color foreground;

    bool create_video = false;
    std::string filename;
};

color rgba_to_color(const Gdk::RGBA& rgba);
Gdk::RGBA color_to_rgba(const color& l_color);

#endif //GRAY_SCOTT_PARAMS_H
