#ifndef GRAY_SCOTT_TEXTURE_H
#define GRAY_SCOTT_TEXTURE_H
#include <vector>
#include <iostream>

#include <GL/glew.h>

struct TexInfo {
    bool updated;
    int width;
    int height;

    std::vector<float>* data_array;

    bool operator!=(TexInfo info) const;
};


class Texture {
public:
    Texture() = default;
    ~Texture() = default;

    void set_info(bool updated, int width, int height, std::vector<float>*);
    void set_info(TexInfo);
    const TexInfo* get_info() const;

    void set_updated(bool);
    bool get_updated() const;

    void bind(GLuint tex);

private:
    TexInfo info = {false, 0, 0, nullptr};
};


#endif //GRAY_SCOTT_TEXTURE_H
