#include "params.h"

color rgba_to_color(const Gdk::RGBA& rgba){
    color l_color;

    l_color.r = rgba.get_red();
    l_color.g = rgba.get_green();
    l_color.b = rgba.get_blue();
    l_color.a = rgba.get_alpha();

    return l_color;
}

Gdk::RGBA color_to_rgba(const color& l_color){
    Gdk::RGBA rgba;

    rgba.set_red(l_color.r);
    rgba.set_green(l_color.g);
    rgba.set_blue(l_color.b);
    rgba.set_alpha(l_color.a);

    return rgba;
}