#include "Texture.h"

bool TexInfo::operator!=(TexInfo info) const{
    if (width != info.width || height != info.width || data_array != info.data_array) return true;
    return false;
}

void Texture::set_info(bool updated, int width, int height, std::vector<float>* data){
    info.updated = updated;
    info.width = width;
    info.height = height;
    info.data_array = data;
}

void Texture::set_info(TexInfo tex_inf){
    info = tex_inf;
}

void Texture::bind(GLuint tex) {
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RED,
                 info.width,
                 info.height,
                 0, GL_RED, GL_FLOAT,
                 info.data_array->data());

    auto x = glGetError();
    if (x != 0){
        std::cout << "Bind: glerror: " <<  x << std::endl;
    }

//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);
}

const TexInfo *Texture::get_info() const {
    return &info;
    // TODO throw error
}

void Texture::set_updated(bool upd) {
    info.updated = upd;
}

bool Texture::get_updated() const {
    return info.updated;
}